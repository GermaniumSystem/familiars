Familiars mod V 1.10
Playable species mod for Starbound Beta v. Angry Koala
Created by Bitcoon
Nightly->Stable update thanks to Geodeek

::Description::
----------------------------------
The Familiars mod lets you play as my own little creations - a race of fuzzy glow dudes made physical by magical forces beyond their understanding.
The sprites for this mod were made from scratch, so this race is decidedly shorter in stature and somewhat differently shaped than other races.
However, this species is fully compatible with others.


::Features::
----------------------------------
-Fully custom sprite work (created from the ground up)
-Tons of colors and styles to choose from
-Compatible with other species mods
-villager NPCs for the species
-gets unique dialogue from every vanilla race
-Nifty respawn animation
-Custom namegen
-(removed) Awesome spaceship
-Unique craftables for low and mid tiers, including armor, guns and broadswords


::Installation::
----------------------------------
1. Install your choice of character creation extension mod. Xander Kau and Kawa's mods are both confirmed to work. You can play without one, but you can't choose a custom species to play as without it.

2. Find your Starbound installation's mod directory. On Steam, the default is C:\Program Files\Steam\SteamApps\Starbound\mods

3. Extract the contents of "Starbound Familiars Mod.zip" into the mods directory. The folder structure should be mods\familiar\... so make sure it isn't mods\blahblah\familiar\...

4. Enjoy!


::Be Aware::
---------------------------------
-Small visual inconsistencies (arm rotation off by a pixel or two, held items not totally aligned, etc) - not a big deal but can't really fix them without breaking other races. Unless the devs give us a way to change sprite rotation points per race (rather than globally) I can't really do much about this.

-The sprites are shorter than most races, so you may have to hide your armor. Transparent vanity items are given at the start, so you can hide anything that doesn't look right.


::Known Issues::
---------------------------------
Compatibility: This mod alters some of the existing village and house "dungeon" files in order to sprinkle in some familiars and may not be compatible with other mods which alter those same files.
Incomplete: Bandits have a file but no known means of spawning. They just won't be included for the time being as they're not a major feature of the mod.


::Changelog::
---------------------------------
V 1.11-
	-Added new weapons - ValuCannon (starter gun) and VapoCannon (top tier gun) and also balanced guns a bit better (currently no way to earn VapoCannon, but you can spawn it)
	-Removed the prison dungeon files for compatibility with the changes to human prison dungeon. No more remarkably-content familiars running around in prison now.
V 1.10-1/30/2015
	-Updated to Geodeek's version from nightlies/unstable branch.
	-Removed unique Familiar ship. I'm not sure whether this will ever be re-implemented, as it simply wasn't built with modularity in mind. 
	-Added unique starter broadsword. Weapons and armor re-balanced to be more in-line with existing item tiers... but likely still not balanced well because I'm not aware of what changes were made to progression.
V 1.02-2/18/2014
	-Pretty much just changed the beta version to Enraged Koala so you can still use the mod.
	-Also changed the captain's chair to loungeable.
	-Fixed brokenness the patch did to the respawn animation.
	-Lights in the Hysteria ship can be broken now, to give a little more room for customization.
V 1.01-1/26/2014
	-Removed Character Creation mod files. You can now use your choice of extension mod.
V 1.0- 1/26/2014
	-Furious Koala support added
	-edited Character Creation mod included with the mod file now. This is a quick patchup fix while I await a good way to implement this species mod.
	-However, I have removed mod dependency with the Character Creation Mod. This means it's possible to install this mod but not be able to select the Familiars species. You're free to try any Furious-supported character creation mod you'd like, though.
	-Removed unnecessary PSD files and stuff that was just upping the download size.
V 0.95-1/23/2014
	-Finally fixed species colors, allowing for complete customization and way less horrible looking randomized ones.
	-To celebrate... MORE COLOR OPTIONS! You can now be your own special snowflake.
	-More silly names for random characters.
	-Improvements to balance on the custom items so they won't be immediately obsolete as soon as you're able to make them.
V 0.9- 1/9/2014
	-Added badass spaceship and a bunch of component parts
	-Additional codex entries flesh out familiars' lore
	-Added new materials crafted using various ores
	-New materials are used to craft 2 tiered versions of new weapons and armor:
		-armor
		-broadsword
		-gun
	-Added familiar NPC spawns to various random villages and things
V 0.6- 1/5/2014
	-Fixed leftover reference to nonexisting materials
	-Fixed villager NPC file
	-Added bandit NPC file
	-Created alternate tail/head style for "female" gender option
	-Added more "hair" styles
	-More namegen names
	-Respawn animation edited to be less wonky
	-Filled in species-specific dialogue for all standard races
	-Codex entries a little more fleshed out
V 0.52-1/5/2014
	-Added invisible hat to starting treasure.
V 0.51-1/5/2014
	-Fixed typo in starting quest so the quest chain won't break.
V 0.5- 1/5/2014
	-First release of Familiars mod.


::Modifications::
---------------------------------
If you would like to use this mod as a basis to create your own, I only ask that you properly attribute the original work if you leave any of my sprites in. Most of what is contained in this file is created from the ground up by me.


::Contact Info::
---------------------------------

thebitcoon@gmail.com
@Bitcoon